<?php

namespace Drupal\Tests\zendesk_forms\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Core\Form\FormState;
use Drupal\zendesk_forms\Form\ZendeskSupportForm;
use Prophecy\Argument;
use Zendesk\API\Resources\Core\Tickets;

/**
 * Tests the support form.
 *
 * @group zendesk_forms
 */
class SupportFormTest extends KernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'user',
    'zendesk_forms',
    'zendesk_forms_mocked_api',
  ];

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->classResolver = $this->container->get('class_resolver');
    $this->formBuilder = $this->container->get('form_builder');
  }

  /**
   * Tests the use of the basic form.
   */
  public function testSupportForm() {
    // The values submitted to the form.
    $form_state = (new FormState())
      ->setValues([
        'name' => 'Disgruntled Customer',
        'email' => 'dis@gruntled.com',
        'subject' => "It doesn't work",
        'description' => "I tried hitting it with a hammer.",
      ]);

    // The values we expect to be sent to Zendesk.
    $zendesk_tickets_resource = $this->prophesize(Tickets::class);
    $zendesk_tickets_resource->create([
      'subject'  => "It doesn't work",
      'comment'  => [
        'body' => "I tried hitting it with a hammer.",
      ],
      'requester' => [
        'name' => 'Disgruntled Customer',
        'email' => 'dis@gruntled.com',
      ],
      'priority' => 'normal',
    ])->shouldBeCalled();

    $this->container->get('zendesk_forms.zendesk_api_client')->setMockTicketsResource($zendesk_tickets_resource->reveal());

    $form_object = $this->classResolver->getInstanceFromDefinition(ZendeskSupportForm::class);

    $this->formBuilder->submitForm($form_object, $form_state);
  }

}
