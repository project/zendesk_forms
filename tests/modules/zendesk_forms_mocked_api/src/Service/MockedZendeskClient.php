<?php

namespace Drupal\zendesk_forms_mocked_api\Service;

use Drupal\zendesk_forms\Service\ZendeskApiClient as RealService;
use Prophecy\Argument;
use Prophecy\Prophet;
use Zendesk\API\HttpClient;
use Zendesk\API\Resources\Core\Tickets;

/**
 * Mocked version of the Zendesk API client wrapper service.
 */
class MockedZendeskClient extends RealService {

  /**
   * Sets a mocked version of the Zendesk API tickets resource.
   *
   * This allows a test to create the mock itself and set its expectations.
   *
   * @param Tickets $tickets_mock
   *   The mocked tickets resource object.
   */
  public function setMockTicketsResource(Tickets $tickets_mock) {
    $this->ticketsMock = $tickets_mock;
  }

  /**
   * Overrides the real service's method to return a mocked Zendesk client.
   */
  public function getZendeskClient() {
    $subdomain = $this->zendeskConfig->get('subdomain');
    $username  = $this->zendeskConfig->get('user_name');
    $token     = $this->zendeskConfig->get('api_token');

    $prophet = new Prophet;

    $zendesk_tickets_resource = $prophet->prophesize(Tickets::class);
    $zendesk_tickets_resource->create(Argument::any())->shouldBeCalled();

    $zendesk_client = $prophet->prophesize(HttpClient::class);
    $zendesk_client->tickets()->willReturn($this->ticketsMock);

    return $zendesk_client->reveal();
  }

}