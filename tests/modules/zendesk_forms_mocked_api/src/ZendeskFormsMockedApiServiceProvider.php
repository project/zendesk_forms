<?php

namespace Drupal\zendesk_forms_mocked_api;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the secret provider service.
 *
 * Note the name of this class is magic for it to be picked up by core's
 * container builder.
 *
 * See https://www.drupal.org/docs/8/api/services-and-dependency-injection/altering-existing-services-providing-dynamic-services
 */
class ZendeskFormsMockedApiServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides the Zendesk API client wrapper.
    $definition = $container->getDefinition('zendesk_forms.zendesk_api_client');
    $definition->setClass('\Drupal\zendesk_forms_mocked_api\Service\MockedZendeskClient');
  }
}
