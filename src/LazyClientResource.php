<?php

namespace Drupal\zendesk_forms;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Zendesk\API\HttpClient;
use Zendesk\API\Resources\ResourceAbstract;

/**
 * Lazy wrapper around the Zendesk API client's resources.
 *
 * This allows the result of specified calls to be cached in Drupal.
 */
class LazyClientResource {

  /**
   * The API calls whose results should be cached.
   *
   * The outer keys are the names of resource methods, which may be nested; the
   * inner keys are the names of methods on resources. Values are arrays
   * containing 'expire' and 'tags', which are the values to pass to
   * CacheBackendInterface::set().
   *
   * TODO: Add a way to override or configure this.
   *
   * @var array
   */
  protected $cacheableCalls = [
    'tickets' => [
      'forms' => [
        'findAll' => [
          'expire' => CacheBackendInterface::CACHE_PERMANENT,
          'tags' => [],
        ],
      ],
    ],
    'ticketFields' => [
      'findAll' => [
        'expire' => CacheBackendInterface::CACHE_PERMANENT,
        'tags' => [],
      ],
    ],
    'brands' => [
      'findAll' => [
        'expire' => CacheBackendInterface::CACHE_PERMANENT,
        'tags' => [],
      ],
    ],
  ];

  /**
   * The resource object this wraps.
   *
   * @var \Zendesk\API\Resources\ResourceAbstract
   */
  protected $wrappedResource;

  /**
   * The array of method calls made on the client to obtain this resource.
   *
   * @see \Zendesk\API\HttpClient::getValidSubResources()
   *
   * @var string[]
   */
  protected $parents;

  /**
   * Constructs a new LazyClientResource.
   *
   * @param \Zendesk\API\HttpClient|\Zendesk\API\Resources\ResourceAbstract $object_called_on
   *  The object that this resource is being obtained from. Either the
   *  configured Zendesk client, or another Zendesk API resource object.
   * @param string $resource_method_name
   *  The name of the resource method to be called on the object.
   * @param array $arguments
   *  Any arguments for the resource method call.
   * @param string[] $parents
   *  (optional) The names of the successive chained method calls made on the
   *  client to obtain $object_called_on. Defaults to an empty array for the
   *  first lazy resource in a chain.
   */
  public function __construct($object_called_on, $resource_method_name, $arguments, $parents = []) {
    $this->wrappedResource = $object_called_on->{$resource_method_name}(...$arguments);

    $parents[] = $resource_method_name;
    $this->parents = $parents;
  }

  /**
   * Passes calls to the wrapped resource, with caching.
   */
  public function __call($name, $arguments) {
    // If the method call we are making on our wrapped resource will give us
    // another resource, return another lazy resource to wrap around it.
    $sub_resources = $this->wrappedResource::getValidSubResources();
    if (isset($sub_resources[$name])) {
      return new static($this->wrappedResource, $name, $arguments, $this->parents);
    }

    $method_chain = $this->parents;
    $method_chain[] = $name;

    $use_cache = NestedArray::keyExists($this->cacheableCalls, $method_chain);
    $cid = 'zendesk_forms:' . implode(':', $method_chain);

    if ($use_cache && $cache = \Drupal::cache()->get($cid)) {
      $result = $cache->data;
    }
    else {
      $result = $this->wrappedResource->{$name}(...$arguments);

      if ($use_cache) {
        \Drupal::cache()->set(
          $cid,
          $result,
          ...array_values(NestedArray::getValue($this->cacheableCalls, $method_chain))
        );
      }
    }

    return $result;
  }

}
