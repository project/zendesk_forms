<?php

namespace Drupal\zendesk_forms\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\zendesk_forms\LazyClientResource;
use Zendesk\API\HttpClient;

/**
 * Wraps around the Zendesk API client.
 */
class ZendeskApiClient {

  /**
   * The Zendesk configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $zendeskConfig;

  /**
   * The API client.
   *
   * @var \Zendesk\API\HttpClient
   */
  protected $zendeskClient;

  /**
   * Creates a ZendeskApiClient instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory
  ) {
    $this->zendeskConfig = $config_factory->get('zendesk_forms.settings');
  }

  /**
   * Passes calls to the Zendesk client, via a lazy resource to allow caching.
   */
  public function __call($name, $arguments) {
    return new LazyClientResource($this->getZendeskClient(), $name, $arguments);
  }

  /**
   * Gets the Zendesk API client.
   *
   * @return \Zendesk\API\HttpClient
   *   The configured client object.
   */
  public function getZendeskClient() {
    if (!isset($this->zendeskClient)) {
      $subdomain = $this->zendeskConfig->get('subdomain');
      $username  = $this->zendeskConfig->get('user_name');
      $token     = $this->zendeskConfig->get('api_token');

      $this->zendeskClient = new HttpClient($subdomain);
      $this->zendeskClient->setAuth('basic', ['username' => $username, 'token' => $token]);
    }

    return $this->zendeskClient;
  }

}
