<?php

namespace Drupal\zendesk_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Error;
use Drupal\zendesk_forms\Service\ZendeskApiClient;
use Drupal\Component\Utility\EmailValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;

/**
 * Provides a form for creating tickets in Zendesk.
 *
 * This may be enhanced to add further fields to the ticket by adding form
 * elements whose keys begin with 'zendesk_'. These may be added by passing in
 * a FormAPI array to FormBuilder::getForm(), as well as the usual method with
 * hook_form_alter(). Note that this currently only works with top-level form
 * elements.
 *
 * Each form element must have one of the following attributes:
 *  - '#zendesk_custom_field_id': The numeric ticket field ID that is used in
 *    the 'custom_fields' property of the ticket. See
 *    https://developer.zendesk.com/rest_api/docs/support/tickets#setting-custom-field-values
 *  - '#zendesk_field_name': The string machine name of a built-in ticket field.
 *    See https://developer.zendesk.com/rest_api/docs/support/tickets#tickets.
 */
class ZendeskSupportForm extends FormBase {

  /**
   * The Zendesk api client service.
   *
   * @var \Drupal\zendesk_forms\Service\ZendeskApiClient
   */
  protected $zendeskApiClient;

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The Email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Creates a ZendeskSupportForm instance.
   *
   * @param \Drupal\zendesk_forms\Service\ZendeskApiClient $zendesk_api_client
   *   The Zendesk api client service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current active user.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The Email validator service.
   */
  public function __construct(
    ZendeskApiClient $zendesk_api_client,
    AccountProxyInterface $current_user,
    EmailValidatorInterface $email_validator
  ) {
    $this->zendeskApiClient = $zendesk_api_client;
    $this->currentUser = $current_user;
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('zendesk_forms.zendesk_api_client'),
      $container->get('current_user'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zendesk_support_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $custom_fields = []) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => t('Your name'),
      '#required' => TRUE,
      '#default_value' => ($this->currentUser->isAuthenticated() ? $this->currentUser->getDisplayName() : ''),
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => t('Your e-mail address'),
      '#required' => TRUE,
      '#default_value' => ($this->currentUser->isAuthenticated() ? $this->currentUser->getEmail() : ''),
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => t('Subject'),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#required' => TRUE,
    ];

    // Add any custom fields passed as arguments to FormBuilder::getForm().
    $form += $custom_fields;

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Send'),
      '#weight' => 99,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->emailValidator->isValid($form_state->getValue('email'))) {
      $form_state->setErrorByName('email', $this->t("The email address is not valid."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ticket_data = [
      'subject'  => $form_state->getValue('subject'),
      'comment'  => [
        'body' => $form_state->getValue('description'),
      ],
      'requester' => [
        'name' => $form_state->getValue('name'),
        'email' => $form_state->getValue('email'),
      ],
      'priority' => 'normal',
    ];

    // Examine any form values whose keys are prefixed with 'zendesk_' and add
    // those to the ticket data.
    $zendesk_form_value_keys = array_filter(array_keys($form_state->getValues()), function($item) {
      return strpos($item, "zendesk_") === 0;
    });
    foreach ($zendesk_form_value_keys as $form_value_key) {
      if (empty($form_state->getValue($form_value_key))) {
        continue;
      }

      $form_element = $form[$form_value_key];

      $value = Html::escape($form_state->getValue($form_value_key));

      // If the form element defines a Zendesk custom field id in its
      // '#zendesk_custom_field_id' attribute, we submit it in the
      // 'custom_fields' property of the ticket.
      if (isset($form_element['#zendesk_custom_field_id'])) {
        $ticket_data['custom_fields'][] = [
          'id' => $form_element['#zendesk_custom_field_id'],
          'value' => $value,
        ];
      }
      // Else we submit it as a regular Zendesk field key, taking the field name
      // from the '#zendesk_field_name' attribute.
      elseif (isset($form_element['#zendesk_field_name'])) {
        $zendesk_field_name = $form_element['#zendesk_field_name'];
        $ticket_data[$zendesk_field_name] = $value;
      }
      else {
        throw new \Exception("Unable to process form value {$form_value_key} as the form element has no zendesk attribute.");
      }
    }

    try {
      $client = $this->zendeskApiClient; //->getZendeskClient();

      $new_ticket = $client->tickets()->create($ticket_data);

      $this->messenger()->addMessage($this->t('Your support request has been submitted.'));
    }
    catch (\Exception $exception) {
      $variables = Error::decodeException($exception);
      $variables['@ticket-data'] = print_r($ticket_data, TRUE);
      $this->getLogger('zendesk_forms')->log(RfcLogLevel::ERROR, "Unable to post support ticket to Zendesk with ticket data '@ticket-data'; exception message was: " . $exception->getMessage(), $variables);

      $this->messenger()->addError($this->t('There was a problem submitting your support request. Please try again later.'));
    }
  }

}
